﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FomeWashAdmin.Models
{
    class ResponseDefault {
        public bool success { get; set; }
        public string msg { get; set; }
    }
    class ResponseLogin
    {
        public bool exito { get; set; }
        public string usuario { get; set; }
        public string nombre { get; set; }
    }

    public class ResponseServicios
    {
        public string IdServicio { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Fecha { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Titulo { get; set; }
    }

    public class ResponseOperadores
    {
        public string IdOperador { get; set; }
        public string Nombre { get; set; }
        public object Foto { get; set; }
        public string Promedio { get; set; }
        public string TotalDeServicios { get; set; }
    }
}
