﻿using Acr.UserDialogs;
using FomeWashAdmin.Models;
using FomeWashAdmin.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FomeWashAdmin.ViewModels
{
	public class MenuViewModel : BindableBase
	{
        public INavigationService navigation;
        public Command NuevaCitaCommand { get; set; }
        public Command AjustesCommand { get; set; }
        public Command MisCitasCommand { get; set; }
        public Command SalirCommand { get; set; }
        public Command ProductosCommand { get; set; }
        public Command PromocionesCommand { get; set; }
        public Command Appearing { get; set; }
        private bool notificaciones;

        public bool Notificaciones
        {
            get { return notificaciones; }
            set { notificaciones = value;
                RaisePropertyChanged();
            }
        }


        public MenuViewModel(INavigationService navigationService) 
        {
            navigation = navigationService;
            NuevaCitaCommand = new Command(async () => await nuevaCita());
            AjustesCommand = new Command(async () => await ajustes());
            MisCitasCommand = new Command(async () => await misCitas());
            SalirCommand = new Command(salir);
            ProductosCommand = new Command(async () => await ProductosAsync());
            PromocionesCommand = new Command(async () => await Promociones());
            Appearing = new Command(async ()=> await getServiciosAsync());

        }

        public async Task nuevaCita()
        {
            await navigation.NavigateAsync("Solicitudes");
        }

        public async Task ajustes()
        {
            /*UserDialogs.Instance.ShowLoading();
            await navigation.NavigateAsync("Ajustes");
            UserDialogs.Instance.HideLoading();*/
        }

        public async Task misCitas()
        {

            /*
            await navigation.NavigateAsync("MisServicios");*/

        }

        public void salir()
        {
            UserDialogs.Instance.ShowLoading();
            Settings.CorreoSettings = string.Empty;
            Settings.IdUsuarioSettings = string.Empty;
            Settings.NombreSettings = string.Empty;
            Settings.PasswordSettings = string.Empty;
            navigation.NavigateAsync("../Login");
            UserDialogs.Instance.HideLoading();
        }

        public async Task ProductosAsync()
        {
            /*
            UserDialogs.Instance.ShowLoading();
            await navigation.NavigateAsync("Productos");
            UserDialogs.Instance.HideLoading();*/
        }

        public async Task Promociones()
        {
            /*UserDialogs.Instance.ShowLoading();
            await navigation.NavigateAsync("Promociones");
            UserDialogs.Instance.HideLoading();*/
        }
        async Task getServiciosAsync() {
            UserDialogs.Instance.ShowLoading();
            List<ResponseServicios> aux=await RestClient.GetServicios();

            if (aux!=null)
            {
                if (aux.Count > 0)
                {
                    Notificaciones = true;
                }
            }
            
            UserDialogs.Instance.HideLoading();
        }
    }
}
