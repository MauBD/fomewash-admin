﻿using FomeWashAdmin.Models;
using FomeWashAdmin.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FomeWashAdmin.ViewModels
{
	public class SplashViewModel : BindableBase
	{
        public Command Appearing { get; set; }
        private double imagescale = 1;
        private INavigationService _navigationService;

        public double ImageScale
        {
            get { return imagescale; }
            set
            {
                imagescale = value;
                RaisePropertyChanged();
            }
        }

        public SplashViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            Appearing = new Command(async () => await appearingAsync());
        }

        async Task appearingAsync()
        {
            await EscalarAsync();
            await Login();
        }

        async Task EscalarAsync()
        {
            for (double i = 0; i <= 1.5; i += .1)
            {
                await Task.Delay(50);
                ImageScale = i;
            }
        }
        async Task Login()
        {

            if (Settings.CorreoSettings != String.Empty)
            {
                ResponseLogin a = await RestClient.login(Settings.CorreoSettings, Settings.PasswordSettings);
                if (a.exito)
                {
                    await _navigationService.NavigateAsync("../Menu");
                }
                else
                {
                    await _navigationService.NavigateAsync("../Login");
                }
            }
            else
            {
                await _navigationService.NavigateAsync("../Login");
            }
        }
    }
}
