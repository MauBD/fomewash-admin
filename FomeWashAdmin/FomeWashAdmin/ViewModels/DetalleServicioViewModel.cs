﻿using Acr.UserDialogs;
using FomeWashAdmin.Models;
using FomeWashAdmin.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using TK.CustomMap;
using Xamarin.Forms;

namespace FomeWashAdmin.ViewModels
{
	public class DetalleServicioViewModel : BindableBase, INavigatingAware
    {
        private MapSpan mapregion;
        public Command Appearing { get; set; }
        private TKCustomMapPin pin;
        private List<TKCustomMapPin> _Pins;
        private ResponseServicios item;
        public Command MapClickedCommand { get; set; }
        public INavigationService _navigationService { get; set; }
        public Command MostrarCancelar { get; set; }
        public Command MostrarAceptar { get; set; }
        private bool _aceptarisvisible;
        private bool _rechazarisvisible;
        private string _comentario;
        private List<ResponseOperadores> operadores;

        public DetalleServicioViewModel(INavigationService navigationService)
        {
            MapRegion = MapSpan.FromCenterAndRadius(new Position(19.0412894, -98.20620129999998), Distance.FromMeters(50));
            Appearing = new Command(async ()=> await appearingAsync());
            MapClickedCommand = new Command(mapclick);
            MostrarCancelar = new Command(_MostrarCancelar);
            MostrarAceptar = new Command(_MostrarAceptar);
            _navigationService = navigationService;
        }

        //Propieades
        public string Comentario
        {
            get { return _comentario; }
            set { _comentario = value;
                RaisePropertyChanged();
            }
        }

        public bool AceptarIsVisible
        {
            get { return _aceptarisvisible; }
            set { _aceptarisvisible = value;
                RaisePropertyChanged();
            }
        }

        public bool RechazarIsVisible
        {
            get { return _rechazarisvisible; }
            set
            {
                _rechazarisvisible = value;
                RaisePropertyChanged();
            }
        }

        public ResponseServicios Item
        {
            get { return item; }
            set
            {
                item = value;
                RaisePropertyChanged();
            }
        }

        public List<TKCustomMapPin> Pins
        {
            get { return _Pins; }
            set
            {
                _Pins = value;
                RaisePropertyChanged();
            }
        }

        public TKCustomMapPin Pin
        {
            get { return pin; }
            set
            {
                pin = value;
                RaisePropertyChanged();
            }
        }

        public MapSpan MapRegion
        {
            get { return mapregion; }
            set
            {
                mapregion = value;
                RaisePropertyChanged();
            }
        }

        public List<ResponseOperadores> SourceOperadores
        {
            get { return operadores; }
            set
            {
                operadores = value;
                RaisePropertyChanged();
            }
        }

        //Metodos
        async Task appearingAsync()
        {
            UserDialogs.Instance.ShowLoading();
            SourceOperadores = await RestClient.GetOperadores();
            Pins = new List<TKCustomMapPin>();
            Pin = new TKCustomMapPin();
            Pin.IsDraggable = false;
            Pin.Position = new Position(Convert.ToDouble(Item.Latitud), Convert.ToDouble(Item.Longitud));
            Pins.Add(Pin);
            MapRegion = MapSpan.FromCenterAndRadius(Pin.Position, Distance.FromMeters(50));
            UserDialogs.Instance.HideLoading();
        }
        
        private void _MostrarCancelar(object obj)
        {
            RechazarIsVisible = true;
            AceptarIsVisible = false;
        }

        private void _MostrarAceptar(object obj)
        {
            AceptarIsVisible = true;
            RechazarIsVisible = false;
        }

        private void mapclick(object obj)
        {
            CultureInfo culture = CultureInfo.InstalledUICulture;
            NumberFormatInfo numberFormatInfo = (NumberFormatInfo)culture.NumberFormat.Clone();
            numberFormatInfo.NumberDecimalSeparator = ".";
            string uri = "https://maps.google.com/?q=";
            double lat = double.Parse(Item.Latitud);
            double lon = double.Parse(Item.Longitud);
            uri += lat.ToString(numberFormatInfo) + "," + lon.ToString(numberFormatInfo) + "&z=14";
            Device.OpenUri(new Uri(uri));
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("solicitud"))
            {
                Item = (ResponseServicios)parameters["solicitud"];
            }
        }
    }
}
