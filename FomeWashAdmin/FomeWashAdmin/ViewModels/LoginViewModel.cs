﻿using FomeWashAdmin.Models;
using FomeWashAdmin.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using FomeWashAdmin.Utilities;
using Acr.UserDialogs;

namespace FomeWashAdmin.ViewModels
{
	public class LoginViewModel : BindableBase
	{
        private INavigationService _navigationService;

        public Command IngresarCommand { get; set; }
        public Command RestoreCommand { get; set; }
        public Command SendMailCommand { get; set; }
        private string usr;
        private string pwd;
        private bool alerta;
        private bool alertpwd;
        private bool alertmail;
        private bool stackrestorevisibility;
        private bool alertmailrestore;
        private string mailrestore;
        private bool alertrestore;

        public LoginViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            IngresarCommand = new Command(async () => await IngresarAsync());
            RestoreCommand = new Command(restore);
            SendMailCommand = new Command(async () => await enviarMailAsync());

        }



        public bool AlertRestore
        {
            get { return alertrestore; }
            set
            {
                alertrestore = value;
                RaisePropertyChanged();
                MailRestore = String.Empty;
                StackRestoreVisibility = false;
            }
        }


        public string MailRestore
        {
            get { return mailrestore; }
            set
            {
                mailrestore = value;
                RaisePropertyChanged();
                alertMailRestore = false;
            }
        }


        public bool alertMailRestore
        {
            get { return alertmailrestore; }
            set
            {
                alertmailrestore = value;
                RaisePropertyChanged();
            }
        }



        public bool StackRestoreVisibility
        {
            get { return stackrestorevisibility; }
            set
            {
                stackrestorevisibility = value;
                RaisePropertyChanged();
            }
        }

        public bool alertA
        {
            get { return alerta; }
            set
            {
                alerta = value;
                RaisePropertyChanged();
            }
        }

        public bool alertPwd
        {
            get { return alertpwd; }
            set
            {
                alertpwd = value;
                RaisePropertyChanged();
            }
        }

        public bool alertMail
        {
            get { return alertmail; }
            set
            {
                alertmail = value;
                RaisePropertyChanged();
            }
        }



        public string USR
        {
            get { return usr; }
            set
            {
                usr = value;
                alertMail = false;
                alertA = false;
                RaisePropertyChanged();
            }
        }

        public string PWD
        {
            get { return pwd; }
            set
            {
                pwd = value;
                alertPwd = false;
                alertA = false;
                RaisePropertyChanged();
            }
        }

        private async Task IngresarAsync()
        {

            if (Utilerias.validarcorreo(USR) && !String.IsNullOrWhiteSpace(PWD))
            {

                UserDialogs.Instance.ShowLoading("Ingresando...");
                ResponseLogin a = await RestClient.login(USR, PWD);
                if (a.exito)
                {

                    Settings.IdUsuarioSettings = a.usuario;
                    Settings.NombreSettings = a.nombre;
                    Settings.CorreoSettings = USR;
                    Settings.PasswordSettings = PWD;
                    await _navigationService.NavigateAsync("../Menu");

                }
                else
                {
                    alertA = true;
                }
            }
            else
            {
                if (!Utilerias.validarcorreo(USR))
                {
                    alertMail = true;
                }
                if (String.IsNullOrWhiteSpace(PWD))
                {
                    alertPwd = true;
                }
            }
            UserDialogs.Instance.HideLoading();
        }

       

        private void restore()
        {
            if (StackRestoreVisibility)
            {
                AlertRestore = false;
                StackRestoreVisibility = false;
            }
            else
            {
                AlertRestore = false;
                StackRestoreVisibility = true;
            }
        }

        public async Task enviarMailAsync()
        {
            if (Utilerias.validarcorreo(MailRestore))
            {
                UserDialogs.Instance.ShowLoading("Enviando...");
                ResponseDefault response = await RestClient.recuperarPwd(MailRestore);
                AlertRestore = true;
                UserDialogs.Instance.HideLoading();

            }
            else
            {
                alertMailRestore = true;
            }
        }
    }
}
