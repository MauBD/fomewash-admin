﻿using Acr.UserDialogs;
using FomeWashAdmin.Models;
using FomeWashAdmin.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FomeWashAdmin.ViewModels
{
	public class SolicitudesViewModel : BindableBase, INavigationAware
    {
        public INavigationService localnavigation { get; set; }
        private List<ResponseServicios> source;
        private DelegateCommand<ResponseServicios> _selCustCommand;
        public DelegateCommand<ResponseServicios> SelectCustomerCommand => _selCustCommand ?? (_selCustCommand = new DelegateCommand<ResponseServicios>(detallesAsync));
        public Command AppearingCommand { get; set; }

        public List<ResponseServicios> ServiciosSource
        {
            get { return source; }
            set
            {
                source = value;
                RaisePropertyChanged();
            }
        }

        private async Task AppearingAsync()
        {
            UserDialogs.Instance.ShowLoading();
            ServiciosSource = await RestClient.GetServicios();
            UserDialogs.Instance.HideLoading();
        }

        public SolicitudesViewModel(INavigationService navigationService)
        {
            localnavigation = navigationService;
            AppearingCommand = new Command(async () => await AppearingAsync());
        }
        private async void detallesAsync(ResponseServicios paramData)
        {
            var parameters = new NavigationParameters
            {
                { "solicitud", paramData }
            };
            await localnavigation.NavigateAsync("DetalleServicio", parameters);
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {

        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {

        }

    }
}
