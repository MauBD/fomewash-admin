﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace FomeWashAdmin.Services
{
    class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string IdUsuarioKey = "IdUsuario_key";
        private static readonly string IdUsuarioDefault = string.Empty;

        private const string Nombre = "nombre_key";
        private static readonly string NombreDefault = string.Empty;

        private const string Foto = "foto_key";
        private static readonly string FotoDefault = "usr.png";

        private const string Correo = "correo_key";
        private static readonly string CorreoDefault = string.Empty;

        private const string Password = "password_key";
        private static readonly string PasswordDefault = string.Empty;


        #endregion


        public static string IdUsuarioSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(IdUsuarioKey, IdUsuarioDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(IdUsuarioKey, value);
            }
        }

        public static string NombreSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(Nombre, NombreDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(Nombre, value);
            }
        }

        public static string FotoSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(Foto, FotoDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(Foto, value);
            }
        }

        public static string CorreoSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(Correo, CorreoDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(Correo, value);
            }
        }

        public static string PasswordSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(Password, PasswordDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(Password, value);
            }
        }
    }
}
