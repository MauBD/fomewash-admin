﻿using FomeWashAdmin.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FomeWashAdmin.Services
{
    class RestClient
    {
        const string baseUrl = "http://www.fomewash.com.mx/panel/Appws/";
        public static async Task<ResponseLogin> login(string usr, string pwd)
        {
            var formContent = new FormUrlEncodedContent(new[]
           {
                   new KeyValuePair<string,string>("usr", usr),
                   new KeyValuePair<string,string>("pwd", pwd)
           });
            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseUrl + "loginAdmin", formContent);

            ResponseLogin data = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject<ResponseLogin>(json);
            }

            return data;
        }

        public static async Task<ResponseDefault> recuperarPwd(string mail)
        {
            var formContent = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("mail", mail)
            });
            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseUrl + "recuperarPwd", formContent);

            ResponseDefault aux = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                aux = JsonConvert.DeserializeObject<ResponseDefault>(json);
            }

            return aux;
        }

        public static async Task<List<ResponseServicios>> GetServicios()
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(baseUrl + "solicitudes");

            List<ResponseServicios> aux = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                aux = JsonConvert.DeserializeObject<List<ResponseServicios>>(json);
            }

            return aux;
        }

        public static async Task<List<ResponseOperadores>> GetOperadores()
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(baseUrl + "operadores");

            List<ResponseOperadores> aux = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                aux = JsonConvert.DeserializeObject<List<ResponseOperadores>>(json);
            }

            return aux;
        }
    }
}
